// signup_login_spec.js

describe('Signup Test', () => {
  beforeEach(() => {
    // Visit the website before each test
    cy.visit('https://next-realworld.vercel.app/');
  });

  it('should navigate to the signup page and sign up a new user', () => {
    // Navigate to the signup page
    cy.contains('Sign up').click();

    // Fill out the signup form
    // cy.get(':nth-child(1) > .form-control').type('chandan9898');
    // cy.get(':nth-child(2) > .form-control').type('chandan9898@realworld.com');
    // cy.get(':nth-child(3) > .form-control').type('password');
    // cy.get('.btn').click();

  });

  it('should navigate to the signup page and cannot sign up as existing user', () => {
    // Navigate to the signup page
    cy.contains('Sign up').click();

    // Fill out the signup form
    cy.get(':nth-child(1) > .form-control').type('chandan9988');
    cy.get(':nth-child(2) > .form-control').type('chandan9988@realworld.com');
    cy.get(':nth-child(3) > .form-control').type('password');
    cy.get('.btn').click();
     
  });

  it('should navigate to the signup page and cannot signup with existing username', () => {
    // Navigate to the signup page
    cy.contains('Sign up').click();

    // Fill out the signup form
    cy.get(':nth-child(1) > .form-control').type('chandan9988');
    cy.get(':nth-child(2) > .form-control').type('chandan77777@realworld.com');
    cy.get(':nth-child(3) > .form-control').type('password');
    cy.get('.btn').click();
     
  });
  it('should navigate to the signup page and cannot sign up with existing email', () => {
    // Navigate to the signup page
    cy.contains('Sign up').click();

    // Fill out the signup form
    cy.get(':nth-child(1) > .form-control').type('chandan66666');
    cy.get(':nth-child(2) > .form-control').type('chandan9988@realworld.com');
    cy.get(':nth-child(3) > .form-control').type('password');
    cy.get('.btn').click();
     
  });

});
  