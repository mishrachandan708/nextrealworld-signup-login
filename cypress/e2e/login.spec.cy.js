
describe('Login Test', () => {
  beforeEach(() => {
    // Visit the website before each test
    cy.visit('https://next-realworld.vercel.app/');
  });
it('should navigate to the login page and log in an existing user', () => {
    // Navigate to the login page
    cy.get(':nth-child(2) > .nav-link').click();

    // Fill out the login form
    cy.get(':nth-child(1) > .form-control').type('chandan9988@realworld.com');
    cy.get(':nth-child(2) > .form-control').type('password');
    cy.get('.btn').click();

    
  });

  it('should navigate to the login page and cannot log in with non registered email', () => {
    // Navigate to the login page
    cy.get(':nth-child(2) > .nav-link').click();

    // Fill out the login form
    cy.get(':nth-child(1) > .form-control').type('chandan@realworld.com');
    cy.get(':nth-child(2) > .form-control').type('password');
    cy.get('.btn').click();

    
  });

  it('should navigate to the login page and cannot log in with wrong password', () => {
    // Navigate to the login page
    cy.get(':nth-child(2) > .nav-link').click();

    // Fill out the login form
    cy.get(':nth-child(1) > .form-control').type('chandan9988@realworld.com');
    cy.get(':nth-child(2) > .form-control').type('abcdef');
    cy.get('.btn').click();

    
  });

  it('should navigate to the login page and cannot log in with blank email', () => {
    // Navigate to the login page
    cy.get(':nth-child(2) > .nav-link').click();

    // Fill out the login form
    // cy.get(':nth-child(1) > .form-control').type('');
    cy.get(':nth-child(2) > .form-control').type('password');
    cy.get('.btn').click();

    
  });

  it('should navigate to the login page and cannot log in with blank password', () => {
    // Navigate to the login page
    cy.get(':nth-child(2) > .nav-link').click();

    // Fill out the login form
    cy.get(':nth-child(1) > .form-control').type('chandan9988@realworld.com');
    // cy.get(':nth-child(2) > .form-control').type('');
    cy.get('.btn').click();

    
  });

  it('should navigate to the login page and cannot log in with invalid email type', () => {
    // Navigate to the login page
    cy.get(':nth-child(2) > .nav-link').click();

    // Fill out the login form
    cy.get(':nth-child(1) > .form-control').type('chandan9988realworld.com');
    cy.get(':nth-child(2) > .form-control').type('password');
    cy.get('.btn').click();

    
  });


});